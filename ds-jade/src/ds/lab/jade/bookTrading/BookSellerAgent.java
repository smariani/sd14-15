/*****************************************************************
 * JADE - Java Agent DEvelopment Framework is a framework to develop multi-agent
 * systems in compliance with the FIPA specifications. Copyright (C) 2000 CSELT
 * S.p.A.
 *
 * GNU Lesser General Public License
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *****************************************************************/

package ds.lab.jade.bookTrading;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * Adapted from Giovanni Caire's Book Trading example in examples.bookTrading
 * within JADE distribution. This is the seller agent, showing how to register
 * to JADE DF in order to offer a service.
 *
 * @author s.mariani@unibo.it
 */
public class BookSellerAgent extends Agent {

    /*
     * Cyclic behaviour waiting for books availability requests. If the
     * requested book is in the local catalogue, the seller agent replies with a
     * PROPOSE message specifying the price. Otherwise a REFUSE message is sent
     * back.
     */
    private class OfferRequestsServer extends Behaviour {

        /** serialVersionUID **/
        private static final long serialVersionUID = 1L;
        /*
         * We are interested in any CALL FOR PROPOSAL message.
         */
        private final MessageTemplate mt = MessageTemplate
                .MatchPerformative(ACLMessage.CFP);

        @Override
        public void action() {

            BookSellerAgent.this.log("Waiting for CFP messages...");
            final ACLMessage msg = this.myAgent.receive(this.mt);
            if (msg != null) {
                BookSellerAgent.this.log("Received CFP '" + msg.getReplyWith()
                        + "' from '" + msg.getSender().getName() + "'.");
                /*
                 * We expect the title of the book as the content of the
                 * message.
                 */
                final String title = msg.getContent();
                final ACLMessage reply = msg.createReply();
                /*
                 * We check availability of the requested book.
                 */
                BookSellerAgent.this.log("Checking availability for book '"
                        + title + "'...");
                final Float price = BookSellerAgent.this.catalogue.get(title);
                if (price != null) {
                    /*
                     * The requested book is available, reply with its price.
                     */
                    BookSellerAgent.this.log("Book '" + title
                            + "' available, proposing price...");
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(String.valueOf(price));
                } else {
                    /*
                     * The requested book is NOT available, reply accordingly.
                     */
                    BookSellerAgent.this.log("Book '" + title
                            + "' NOT available, informing client...");
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("not-available");
                }
                this.myAgent.send(reply);
            } else {
                this.block();
            }

        }

        @Override
        public boolean done() {
            return BookSellerAgent.this.catalogue.isEmpty();
        }

        @Override
        public int onEnd() {
            BookSellerAgent.this.log("Terminating...");
            this.myAgent.doDelete();
            return super.onEnd();
        }

    }

    /*
     * Cyclic behaviour waiting for books purchase requests. The seller agent
     * removes the purchased book from its catalogue and replies with an INFORM
     * message to notify the buyer that the purchase has been successfully
     * completed.
     */
    private class PurchaseOrdersServer extends Behaviour {

        /** serialVersionUID **/
        private static final long serialVersionUID = 1L;
        /*
         * We are interested in any ACCEPT PROPOSAL message.
         */
        private final MessageTemplate mt = MessageTemplate
                .MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);

        @Override
        public void action() {

            BookSellerAgent.this.log("Waiting for purchase orders...");
            final ACLMessage msg = this.myAgent.receive(this.mt);
            if (msg != null) {
                BookSellerAgent.this.log("Received purchase order '"
                        + msg.getReplyWith() + "' from '"
                        + msg.getSender().getName() + "'.");
                /*
                 * We expect the title of the book as the content of the
                 * message.
                 */
                final String title = msg.getContent();
                final ACLMessage reply = msg.createReply();
                final Float price = BookSellerAgent.this.catalogue
                        .remove(title);
                /*
                 * The requested book may be sold to another buyer in the
                 * meanwhile...
                 */
                if (price != null) {
                    BookSellerAgent.this.log("Selling book '" + title
                            + "' to agent '" + msg.getSender().getName()
                            + "'...");
                    reply.setPerformative(ACLMessage.INFORM);
                } else {
                    BookSellerAgent.this.log("Sorry, book '" + title
                            + "' is not available anymore.");
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("not-available");
                }
                this.myAgent.send(reply);
            } else {
                this.block();
            }

        }

        @Override
        public boolean done() {
            return BookSellerAgent.this.catalogue.isEmpty();
        }

        @Override
        public int onEnd() {
            BookSellerAgent.this.log("Terminating...");
            this.myAgent.doDelete();
            return super.onEnd();
        }

    }

    /** serialVersionUID **/
    private static final long serialVersionUID = 1L;

    /*
     * The catalogue of books for sale (maps the title of a book to its price).
     */
    private Hashtable<String, Float> catalogue;

    /*
     * Just reads books and random prices from an input file.
     */
    private void bootCatalogue() {
        try (final BufferedReader br = new BufferedReader(new FileReader(
                "bin/ds/lab/jade/bookTrading/books.cat"));) {
            String line;
            StringTokenizer st;
            String title;
            LinkedList<Float> prices;
            line = br.readLine();
            while (line != null) {
                st = new StringTokenizer(line, ";");
                title = st.nextToken();
                prices = new LinkedList<Float>();
                while (st.hasMoreTokens()) {
                    prices.add(Float.parseFloat(st.nextToken()));
                }
                this.catalogue.put(
                        title,
                        prices.get((int) Math.round(Math.random()
                                * (prices.size() - 1))));
                line = br.readLine();
            }
        } catch (final FileNotFoundException e) {
            e.printStackTrace();
            this.doDelete();
        } catch (final IOException e) {
            e.printStackTrace();
            this.doDelete();
        }
    }

    private void log(final String msg) {
        System.out.println("[" + this.getName() + "]: " + msg);
    }

    private void printCatalogue() {
        final Enumeration<String> keys = this.catalogue.keys();
        String key;
        this.log("My catalogue is:");
        for (int i = 0; i < this.catalogue.size(); i++) {
            key = keys.nextElement();
            System.out.println("	title: " + key + " price: "
                    + this.catalogue.get(key));
        }
    }

    @Override
    protected void setup() {

        this.log("I'm started.");
        this.catalogue = new Hashtable<String, Float>();
        /*
         * Boot catalogue from .catalog file (drawing random prices) and print
         * out the outcome.
         */
        this.bootCatalogue();
        this.printCatalogue();

        /*
         * 1- Create the agent description.
         */
        final DFAgentDescription dfd = new DFAgentDescription();
        /*
         * 2- Fill its mandatory fields.
         */
        dfd.setName(this.getAID());
        /*
         * 3- Create the service description.
         */
        final ServiceDescription sd = new ServiceDescription();
        /*
         * 4- Fill its mandatory fields.
         */
        sd.setType("book-selling");
        sd.setName("JADE-book-trading");
        /*
         * 5- Add the service description to the agent description.
         */
        dfd.addServices(sd);
        try {
            /*
             * 6- Register the service (through the agent description multiple
             * services can be registered in one shot).
             */
            this.log("Registering '" + sd.getType() + "' service named '"
                    + sd.getName() + "'" + "to the default DF...");
            DFService.register(this, dfd);
        } catch (final FIPAException fe) {
            fe.printStackTrace();
        }

        /*
         * Add the behaviour serving queries from buyer agents.
         */
        this.addBehaviour(new OfferRequestsServer());
        /*
         * Add the behaviour serving purchase orders from buyer agents.
         */
        this.addBehaviour(new PurchaseOrdersServer());

    }

    /*
     * Remember to deregister the services offered by the agent upon shutdown,
     * because the JADE platform does not do it by itself!
     */
    @Override
    protected void takeDown() {
        try {
            this.log("De-registering myself from the default DF...");
            DFService.deregister(this);
        } catch (final FIPAException fe) {
            fe.printStackTrace();
        }
        this.log("I'm done.");
    }

}
