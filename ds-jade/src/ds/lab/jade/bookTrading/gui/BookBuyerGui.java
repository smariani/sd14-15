/*****************************************************************
JADE - Java Agent DEvelopment Framework is a framework to develop
multi-agent systems in compliance with the FIPA specifications.
Copyright (C) 2000 CSELT S.p.A.

GNU Lesser General Public License

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation,
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA  02111-1307, USA.
 *****************************************************************/

package ds.lab.jade.bookTrading.gui;

import jade.gui.GuiEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Adapted from Giovanni Caire's Book Trading example in examples.bookTrading
 * within JADE distribution. This is the buyer agent GUI, showing the correct
 * implementation of JADE Agents - Java Swing GUIs interactions.
 *
 * @author s.mariani@unibo.it
 */
class BookBuyerGui extends JFrame {

    public static final int KILL = 2;
    /*
     * GuiEvent constants.
     */
    public static final int PURCHASE = 1;
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /*
     * From JADE programmer's guide: "In general, it is not a good thing that an
     * external software component maintain a direct object reference to an
     * agent, because this component could directly call any public method of
     * the agent, skipping the asynchronous message passing layer and turning an
     * autonomous agent into a server object, slave to its caller. A better
     * approach would be to gather all the external methods into an interface,
     * implemented by the agent class, then an object reference of that
     * interface will be passed to the GUI so that only the external methods
     * will be available from event handlers.
     */
    private final IBookBuyer myAgent;
    /*
     * The title of the nook to buy.
     */
    private final JTextField titleField;

    public BookBuyerGui(final IBookBuyer a) {

        super(a.getAgentName());
        this.myAgent = a;

        JPanel p = new JPanel();
        p.add(new JLabel("Book title: "));
        this.titleField = new JTextField(50);
        p.add(this.titleField);
        this.getContentPane().add(p, BorderLayout.CENTER);

        /*
         * Pass to the agent the book title to buy.
         */
        final JButton purchaseButton = new JButton("Purchase");
        purchaseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent ev) {
                final GuiEvent ge = new GuiEvent(purchaseButton,
                        BookBuyerGui.PURCHASE);
                ge.addParameter(BookBuyerGui.this.titleField.getText().trim());
                BookBuyerGui.this.myAgent.postGuiEvent(ge);
            }
        });
        p = new JPanel();
        p.add(purchaseButton);
        this.getContentPane().add(p, BorderLayout.SOUTH);

        /*
         * Make the agent terminate when the user closes the GUI using the
         * button on the upper right corner.
         */
        final BookBuyerGui me = this;
        this.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(final WindowEvent e) {
                final GuiEvent ge = new GuiEvent(me, BookBuyerGui.KILL);
                BookBuyerGui.this.myAgent.postGuiEvent(ge);
            }
        });

        this.setResizable(false);

    }

    public void hideGui() {
        super.setVisible(false);
        this.titleField.setText("");
    }

    public void showGui() {
        this.pack();
        final Dimension screenSize = Toolkit.getDefaultToolkit()
                .getScreenSize();
        final int centerX = (int) screenSize.getWidth() / 2;
        final int centerY = (int) screenSize.getHeight() / 2;
        this.setLocation(centerX - this.getWidth() / 2,
                centerY - this.getHeight() / 2);
        super.setVisible(true);
    }

}
