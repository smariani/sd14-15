/*****************************************************************
JADE - Java Agent DEvelopment Framework is a framework to develop
multi-agent systems in compliance with the FIPA specifications.
Copyright (C) 2000 CSELT S.p.A.

GNU Lesser General Public License

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation,
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA  02111-1307, USA.
 *****************************************************************/

package ds.lab.jade.mobility;

import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.domain.FIPANames;
import jade.domain.JADEAgentManagement.QueryPlatformLocationsAction;
import jade.domain.mobility.MobilityOntology;
import jade.lang.acl.ACLMessage;
import jade.proto.SimpleAchieveREInitiator;

/**
 * This behaviour extends SimpleAchieveREInitiator in order to request to the
 * AMS the list of available locations where the agent can move/clone. Then, it
 * displays these locations on the GUI. NB: SimpleAchieveREInitiator is a
 * simplified version (less methods to override) of the AchieveREInitiator
 * behaviour.
 *
 * @author Fabio Bellifemine - CSELT S.p.A.
 */
public class GetLocations extends SimpleAchieveREInitiator {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final ACLMessage request;

    /*
     * Custom constructor to instatiate the general purpose
     * SimpleAchieveREInitiator to our needs.
     */
    public GetLocations(final MobileAgent a) {
        /*
         * Call the constructor of FipaRequestInitiatorBehaviour.
         */
        super(a, new ACLMessage(ACLMessage.REQUEST));
        /*
         * Method 'getDataStore()' gets the HashMap associated to the behaviour
         * where all messages are stored. 'REQUEST_KEY' constant serves to
         * retrieve the REQUEST ACLMessage passed in the constructor.
         */
        this.request = (ACLMessage) this.getDataStore().get(this.REQUEST_KEY);
        /*
         * Fill needed parameters.
         */
        this.request.clearAllReceiver();
        this.request.addReceiver(a.getAMS());
        this.request.setLanguage(FIPANames.ContentLanguage.FIPA_SL0);
        this.request.setOntology(MobilityOntology.NAME);
        this.request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
        /*
         * Create the Action to request and encode it into the ACL message.
         */
        try {
            final Action action = new Action();
            action.setActor(a.getAMS());
            action.setAction(new QueryPlatformLocationsAction());
            a.getContentManager().fillContent(this.request, action);
        } catch (final Exception fe) {
            fe.printStackTrace();
        }
        // Reset the created request for next iterations.
        // reset(request);
    }

    @Override
    protected void handleAgree(final ACLMessage msg) {
        /*
         * not used atm
         */
    }

    @Override
    protected void handleFailure(final ACLMessage msg) {
        /*
         * not used atm
         */
    }

    /*
     * We care only about inform messages, that is the response from the AMS
     * conveying the available locations.
     */
    @Override
    protected void handleInform(final ACLMessage inform) {
        try {
            final Result results = (Result) this.myAgent.getContentManager()
                    .extractContent(inform);
            /*
             * Update the GUI.
             */
            ((MobileAgent) this.myAgent).updateLocations(results.getItems()
                    .iterator());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void handleNotUnderstood(final ACLMessage msg) {
        /*
         * not used atm
         */
    }

    @Override
    protected void handleRefuse(final ACLMessage msg) {
        /*
         * not used atm
         */
    }

}
