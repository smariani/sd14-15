package ds.lab.jade.mobility;

import jade.gui.GuiEvent;

/**
 * External interface of mobile agents, providing methods which can be safely
 * called in OO style.
 * 
 * @author (contributor) Ste
 */
public interface MobileAgentExternal {

    /*
     * GUI events constants.
     */
    int CLONE_EVENT = 1002;
    int EXIT = 1000;
    int MOVE_EVENT = 1001;
    int REFRESH_EVENT = 1005;

    /*
     * Post a GUI event to the agent queue.
     */
    void queueGuiEvent(GuiEvent e);

    /*
     * Get the local name of the agent.
     */
    String who();

}
