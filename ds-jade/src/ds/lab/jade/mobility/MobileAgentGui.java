/*****************************************************************
JADE - Java Agent DEvelopment Framework is a framework to develop
multi-agent systems in compliance with the FIPA specifications.
Copyright (C) 2000 CSELT S.p.A.

GNU Lesser General Public License

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation,
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA  02111-1307, USA.
 *****************************************************************/

package ds.lab.jade.mobility;

import jade.core.Location;
import jade.gui.GuiEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;

/**
 * Adapted from Giovanni Caire's Mobility example in examples.mobile within JADE
 * distribution. This is the GUI of the MobileAgent. Because in Java a new
 * Thread is spawn for each GUI, the communication with the Agent thread is
 * based on event passing.
 *
 * @author s.mariani@unibo.it
 */
public class MobileAgentGui extends JFrame implements ActionListener {

    private static final String CLONELABEL = "CLONE";
    private static final String EXITLABEL = "EXIT";
    private static final String MOVELABEL = "MOVE";
    private static final String REFRESHLABEL = "Refresh available locations";
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final JTable availableSiteList;
    private final LocationTableModel availableSiteListModel;

    private final JTextField counterText;
    private final MobileAgentExternal myAgent;
    private final JTable visitedSiteList;
    private final LocationTableModel visitedSiteListModel;

    public MobileAgentGui(final MobileAgentExternal mobileAgentExternal) {

        super();

        this.myAgent = mobileAgentExternal;
        this.setTitle("GUI of " + mobileAgentExternal.who());
        this.setSize(505, 405);

        final JPanel main = new JPanel();
        main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
        final JPanel counterPanel = new JPanel();
        counterPanel.setLayout(new BoxLayout(counterPanel, BoxLayout.X_AXIS));
        final JLabel counterLabel = new JLabel("Counter value: ");
        this.counterText = new JTextField();
        counterPanel.add(counterLabel);
        counterPanel.add(this.counterText);
        main.add(counterPanel);

        this.availableSiteListModel = new LocationTableModel();
        this.availableSiteList = new JTable(this.availableSiteListModel);
        this.availableSiteList
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        final JPanel availablePanel = new JPanel();
        availablePanel.setLayout(new BorderLayout());
        final JScrollPane avPane = new JScrollPane();
        avPane.getViewport().setView(this.availableSiteList);
        availablePanel.add(avPane, BorderLayout.CENTER);
        availablePanel.setBorder(BorderFactory
                .createTitledBorder("Available Locations"));
        this.availableSiteList.setRowHeight(20);
        main.add(availablePanel);

        TableColumn c;
        c = this.availableSiteList.getColumn(this.availableSiteList
                .getColumnName(0));
        c.setHeaderValue("ID");
        c = this.availableSiteList.getColumn(this.availableSiteList
                .getColumnName(1));
        c.setHeaderValue("Name");
        c = this.availableSiteList.getColumn(this.availableSiteList
                .getColumnName(2));
        c.setHeaderValue("Protocol");
        c = this.availableSiteList.getColumn(this.availableSiteList
                .getColumnName(3));
        c.setHeaderValue("Address");

        final JPanel visitedPanel = new JPanel();
        visitedPanel.setLayout(new BorderLayout());
        this.visitedSiteListModel = new LocationTableModel();
        this.visitedSiteList = new JTable(this.visitedSiteListModel);
        final JScrollPane pane = new JScrollPane();
        pane.getViewport().setView(this.visitedSiteList);
        visitedPanel.add(pane, BorderLayout.CENTER);
        visitedPanel.setBorder(BorderFactory
                .createTitledBorder("Visited Locations"));
        this.visitedSiteList.setRowHeight(20);
        main.add(visitedPanel);

        c = this.visitedSiteList.getColumn(this.visitedSiteList
                .getColumnName(0));
        c.setHeaderValue("ID");
        c = this.visitedSiteList.getColumn(this.visitedSiteList
                .getColumnName(1));
        c.setHeaderValue("Name");
        c = this.visitedSiteList.getColumn(this.visitedSiteList
                .getColumnName(2));
        c.setHeaderValue("Protocol");
        c = this.visitedSiteList.getColumn(this.visitedSiteList
                .getColumnName(3));
        c.setHeaderValue("Address");

        final JPanel p = new JPanel();
        JButton b = new JButton(MobileAgentGui.REFRESHLABEL);
        b.addActionListener(this);
        p.add(b);
        b = new JButton(MobileAgentGui.MOVELABEL);
        b.addActionListener(this);
        p.add(b);
        b = new JButton(MobileAgentGui.CLONELABEL);
        b.addActionListener(this);
        p.add(b);
        b = new JButton(MobileAgentGui.EXITLABEL);
        b.addActionListener(this);
        p.add(b);
        main.add(p);

        this.getContentPane().add(main, BorderLayout.CENTER);

    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        final String command = e.getActionCommand();
        if (command.equalsIgnoreCase(MobileAgentGui.MOVELABEL)) {
            Location dest;
            final int sel = this.availableSiteList.getSelectedRow();
            if (sel >= 0) {
                dest = this.availableSiteListModel.getElementAt(sel);
            } else {
                dest = this.availableSiteListModel.getElementAt(0);
            }
            final GuiEvent ev = new GuiEvent(this,
                    MobileAgentExternal.MOVE_EVENT);
            ev.addParameter(dest);
            this.myAgent.queueGuiEvent(ev);
        } else if (command.equalsIgnoreCase(MobileAgentGui.CLONELABEL)) {
            Location dest;
            final int sel = this.availableSiteList.getSelectedRow();
            if (sel >= 0) {
                dest = this.availableSiteListModel.getElementAt(sel);
            } else {
                dest = this.availableSiteListModel.getElementAt(0);
            }
            final GuiEvent ev = new GuiEvent(this,
                    MobileAgentExternal.CLONE_EVENT);
            ev.addParameter(dest);
            this.myAgent.queueGuiEvent(ev);
        } else if (command.equalsIgnoreCase(MobileAgentGui.EXITLABEL)) {
            final GuiEvent ev = new GuiEvent(null, MobileAgentExternal.EXIT);
            this.myAgent.queueGuiEvent(ev);
        } else if (command.equalsIgnoreCase(MobileAgentGui.REFRESHLABEL)) {
            final GuiEvent ev = new GuiEvent(null,
                    MobileAgentExternal.REFRESH_EVENT);
            this.myAgent.queueGuiEvent(ev);
        }
    }

    /*
     * Called by the MobileAgent as soon as it gets to its destination location.
     */
    public void addVisitedSite(final Location site) {
        this.visitedSiteListModel.add(site);
        this.visitedSiteListModel.fireTableDataChanged();
    }

    /*
     * Called by the MobileAgent every second to show time passing.
     */
    public void displayCounter(final int value) {
        this.counterText.setText(Integer.toString(value));
    }

    public void showGui() {
        this.pack();
        final Dimension screenSize = Toolkit.getDefaultToolkit()
                .getScreenSize();
        final int centerX = (int) screenSize.getWidth() / 2;
        final int centerY = (int) screenSize.getHeight() / 2;
        this.setLocation(centerX - this.getWidth() / 2,
                centerY - this.getHeight() / 2);
        this.setVisible(true);
    }

    /*
     * Called by the MobileAgent upon receiption of an INFORM message from the
     * AMS.
     */
    public void updateLocations(final Iterator<?> list) {
        this.availableSiteListModel.clear();
        while (list.hasNext()) {
            final Object obj = list.next();
            this.availableSiteListModel.add((Location) obj);
        }
        this.availableSiteListModel.fireTableDataChanged();
    }

}
