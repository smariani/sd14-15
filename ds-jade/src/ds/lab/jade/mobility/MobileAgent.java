/*****************************************************************
JADE - Java Agent DEvelopment Framework is a framework to develop
multi-agent systems in compliance with the FIPA specifications.
Copyright (C) 2000 CSELT S.p.A.

GNU Lesser General Public License

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation,
version 2.1 of the License.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA  02111-1307, USA.
 *****************************************************************/

package ds.lab.jade.mobility;

import jade.content.lang.sl.SLCodec;
import jade.core.Location;
import jade.core.behaviours.Behaviour;
import jade.domain.FIPANames;
import jade.domain.mobility.MobilityOntology;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import java.util.Iterator;
import java.util.Vector;

/**
 * Adapted from Giovanni Caire's Mobility example in examples.mobile within JADE
 * distribution. A moving/cloning agent with a GUI to react to user
 * moving/cloning commands. While visiting locations, it counts seconds elapsed
 * and stores location names.
 *
 * @author s.mariani@unibo.it
 */
public class MobileAgent extends GuiAgent implements MobileAgentExternal {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /*
     * Destination location.
     */
    private Location destination;
    /*
     * NB: 'transient' tells Java Serialization mechanism not to attempt to
     * serialize the object. In this case, we do not serialize the GUI 'cause it
     * will be re-created on the destination JVM (container).
     */
    private transient MobileAgentGui gui;
    /*
     * Counts visited locations.
     */
    private int secs;
    /*
     * Keeps track of visited locations.
     */
    private final Vector<Location> visitedLocations = new Vector<Location>();

    /*
     * Displays the counter on the GUI.
     */
    public void displayCounter() {
        this.gui.displayCounter(this.secs);
    }

    /*
     * Increase counter.
     */
    public void inc() {
        this.secs++;
    }

    @Override
    public void queueGuiEvent(final GuiEvent e) {
        this.onGuiEvent(e);
    }

    @Override
    public void setup() {
        this.log("I'm started.");
        /*
         * Register the SL0 content language.
         */
        this.getContentManager().registerLanguage(new SLCodec(),
                FIPANames.ContentLanguage.FIPA_SL0);
        /*
         * Register the mobility ontology.
         */
        this.getContentManager().registerOntology(
                MobilityOntology.getInstance());
        /*
         * Create and show the GUI passing only the external reference of the
         * agent.
         */
        this.log("Spawning GUI...");
        this.gui = new MobileAgentGui(this);
        this.gui.showGui();
        /*
         * Behaviour which gets the list of available locations and shows it on
         * the GUI.
         */
        this.log("Looking for available locations...");
        this.addBehaviour(new GetLocations(this));
        /*
         * Counting seconds behaviour.
         */
        final Behaviour counter = new SecondsBehaviour(this, 1000);
        this.addBehaviour(counter);

        this.secs = 0;
    }

    @Override
    public void takeDown() {
        this.log("I'm done, killing GUI...");
        if (this.gui != null) {
            this.gui.dispose();
        }
    }

    /*
     * Updates available locations.
     */
    public void updateLocations(final Iterator<?> it) {
        this.gui.updateLocations(it);
    }

    @Override
    public String who() {
        return this.getLocalName();
    }

    private void log(final String msg) {
        System.out.println("[" + this.getName() + "]: " + msg);
    }

    @Override
    protected void afterClone() {
        this.afterMove();
    }

    /*
     * Executed as soon as the agent arrives to the new destination. We display
     * a new GUI and update the list of visited locations and that of available
     * locations.
     */
    @Override
    protected void afterMove() {
        this.log("Booting GUI on new host...");
        this.gui = new MobileAgentGui(this);
        this.visitedLocations.addElement(this.destination);
        for (int i = 0; i < this.visitedLocations.size(); i++) {
            this.gui.addVisitedSite(this.visitedLocations.elementAt(i));
        }
        this.gui.showGui();
        /*
         * Remind to register the SL0 content language and JADE mobility
         * ontology on the new site, since they don't migrate.
         */
        this.getContentManager().registerLanguage(new SLCodec(),
                FIPANames.ContentLanguage.FIPA_SL0);
        this.getContentManager().registerOntology(
                MobilityOntology.getInstance());
        /*
         * Update the list of available locations.
         */
        this.log("Refreshing available locations...");
        this.addBehaviour(new GetLocations(this));
    }

    @Override
    protected void beforeClone() {
        /*
         * not used atm
         */
    }

    /*
     * Executed just before moving the agent to another location. Automatically
     * called by JADE. We dispose the GUI.
     */
    @Override
    protected void beforeMove() {
        this.log("Killing GUI prior to movement...");
        this.gui.dispose();
    }

    @Override
    protected void onGuiEvent(final GuiEvent ev) {
        switch (ev.getType()) {
            case EXIT:
                this.gui.dispose();
                this.gui = null;
                this.doDelete();
                break;
            case MOVE_EVENT:
                final Iterator<?> moveParameters = ev.getAllParameter();
                this.destination = (Location) moveParameters.next();
                this.doMove(this.destination);
                break;
            case CLONE_EVENT:
                final Iterator<?> cloneParameters = ev.getAllParameter();
                this.destination = (Location) cloneParameters.next();
                this.doClone(this.destination, "clone-" + this.secs + "-of-"
                        + this.getLocalName());
                break;
            case REFRESH_EVENT:
                this.addBehaviour(new GetLocations(this));
                break;
            default:
                break;
        }
    }

}
