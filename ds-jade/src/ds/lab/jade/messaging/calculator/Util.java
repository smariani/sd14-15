package ds.lab.jade.messaging.calculator;

import java.util.LinkedList;
import java.util.StringTokenizer;

/**
 * Utility methods to parse message content, generate random values and pick a
 * random ontology.
 *
 * @author s.mariani@unibo.it
 */
public final class Util {

    private Util() {
        /*
         * To avoid instatiation
         */
    }

    /*
     * Ontologies that the Calculator Agent understands.
     */
    private static String[] choices = new String[] { "summation",
            "subtraction", "multiplication", "division" };

    public static String drawnRandomOntology() {
        return Util.choices[(int) Math.round(Math.random() * 3)];
    }

    public static String formatValues(final Float[] values) {
        String formatted = "";
        for (final Float value : values) {
            formatted += value + ",";
        }
        return formatted.substring(0, formatted.length() - 1);
    }

    public static Float[] genRandomValues(final int max, final int num) {
        final Float[] values = new Float[num];
        for (int i = 0; i < num; i++) {
            values[i] = (float) (Math.random() * max);
        }
        return values;
    }

    /*
     * Message content must be a list of comma-separated, double values.
     */
    public static LinkedList<Float> parse(final String toParse)
            throws NumberFormatException {
        final StringTokenizer st = new StringTokenizer(toParse, ",");
        final LinkedList<Float> values = new LinkedList<>();
        while (st.hasMoreTokens()) {
            values.add(Float.parseFloat(st.nextToken()));
        }
        return values;
    }

}
