package ds.lab.jade.behaviours;

/**
 * ***************************************************************
 * JADE - Java Agent DEvelopment Framework is a framework to develop
 * multi-agent systems in compliance with the FIPA specifications.
 * Copyright (C) 2000 CSELT S.p.A.
 *
 * GNU Lesser General Public License
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation,
 * version 2.1 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 * **************************************************************
 */

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.SequentialBehaviour;

/**
 * Adapted from Giovanni Caire's ComplexBehaviourAgent in examples.behaviours
 * within JADE distribution. A not-so-complex agent showing how to nest
 * behaviours to obtain complex patterns.
 *
 * @author s.mariani@unibo.it
 */
public class CompositeBehavioursAgent extends Agent {

    /*
     * Needed due to custom constructor.
     */
    private class MyOneShotBehaviour extends OneShotBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private final int opCode;

        public MyOneShotBehaviour(final int i) {
            this.opCode = i;
        }

        @Override
        public void action() {
            CompositeBehavioursAgent.this.log("Doing operation " + this.opCode
                    + "...");
        }

    }

    /*
     * Needed to terminate properly.
     */
    private class MyParallelBehaviour extends ParallelBehaviour {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public MyParallelBehaviour(final Agent agent, final int condition) {
            super(agent, condition);
        }

        @Override
        public int onEnd() {
            CompositeBehavioursAgent.this.log("Terminated.");
            this.myAgent.doDelete();
            return super.onEnd();
        }

    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*
     * To ease configuration.
     */
    private void configureMySequential(final int i,
            final SequentialBehaviour mySeq) {
        mySeq.addSubBehaviour(new MyOneShotBehaviour(i));
        mySeq.addSubBehaviour(new MyOneShotBehaviour(i + 1));
        mySeq.addSubBehaviour(new MyOneShotBehaviour(i + 2));
    }

    private void log(final String msg) {
        System.out.println("[" + this.getLocalName() + "]: " + msg);
    }

    @Override
    protected void setup() {
        this.log("Started.");
        /*
         * We add a default <SequentialBehaviour> composed by three One-Shot
         * sub-behaviours.
         */
        final SequentialBehaviour sequence = new SequentialBehaviour(this);
        sequence.addSubBehaviour(new OneShotBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void action() {
                CompositeBehavioursAgent.this
                        .log("Doing sequential operation 1...");
            }
        });
        sequence.addSubBehaviour(new OneShotBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void action() {
                CompositeBehavioursAgent.this
                        .log("Doing sequential operation 2...");
            }
        });
        sequence.addSubBehaviour(new OneShotBehaviour(this) {

            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            public void action() {
                CompositeBehavioursAgent.this
                        .log("Doing sequential operation 3...");
            }

            /*
             * Try to uncomment <doDelete()>: what do you expect?
             */
            @Override
            public int onEnd() {
                // myAgent.doDelete();
                return super.onEnd();
            }
        });
        this.addBehaviour(sequence);
        /*
         * Now a custom <ParallelBehaviour> composed by three default Sequential
         * Behaviours.
         */
        final ParallelBehaviour parallel = new MyParallelBehaviour(this,
                ParallelBehaviour.WHEN_ALL);
        /*
         * First <SequentialBehaviour>.
         */
        SequentialBehaviour mySequential = new SequentialBehaviour();
        this.configureMySequential(10, mySequential);
        parallel.addSubBehaviour(mySequential);
        /*
         * Second <SequentialBehaviour>.
         */
        mySequential = new SequentialBehaviour();
        this.configureMySequential(100, mySequential);
        parallel.addSubBehaviour(mySequential);
        /*
         * Third <SequentialBehaviour>.
         */
        mySequential = new SequentialBehaviour();
        this.configureMySequential(1000, mySequential);
        parallel.addSubBehaviour(mySequential);
        /*
         * Now add them to the parallel one.
         */
        this.addBehaviour(parallel);
    }

}
